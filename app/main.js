var container, stats;
var camera, scene, renderer;
var mouseX = 0,
    mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var gui;
var uniforms;

var params = {
    fog: {
        foggy: true,
        near: 1,
        far: 1000,
    },
    rain: {
        rainy: false,
        position: new THREE.Vector3(0, 500, 0),
        positionRandomness: 1000,
        velocity: new THREE.Vector3(0, -2, 0),
        color: 0x0000FF,
        colorRandomness: .2,
        turbulence: 0,
        lifetime: 2,
        size: 10,
        maxParticles: 180000,
        spawnerOptions: {
            spawnRate: 90000,
            timeScale: 1
        },
    },
    cloudy: true,
    day: true,
    house_show: true,
};

var cloud_options = {
    time: {
        value: 1
    },
    scale: {
        value: 3
    },
    cloudColor: {
        value: new THREE.Color(0xffffff)
    },
    backgroundColor: {
        value: new THREE.Color(0xa4c5fc)
    },
    texture: {
        value: null
    },
    red_weight: {
        value: 0.3
    },
    green_weight: {
        value: 0.5
    },
    blue_weight: {
        value: 1
    },
};

var loader_options = {
    grass_texture: 'textures/terrain/grasslight-big.jpg',
    perlin_noise: 'textures/perlin-512.png',
    house_mtl: 'untitled.mtl',
    house_obj: 'untitled.obj',
};

init();
build_gui();
animate();

function init_rain() {
    rainParticles = new Rain({
        maxParticles: params.rain.maxParticles,
    });
}

function init_light() {
    var ambientLight = new THREE.AmbientLight(scene.background);
    scene.add(ambientLight);
    var pointLight = new THREE.PointLight(0xffffff, 0.8);
    var light = new THREE.DirectionalLight(0xdfebff, 0.2);
    light.position.set(50, 200, 100);
    light.position.multiplyScalar(1.3);
    light.castShadow = true;
    light.shadow.mapSize.width = 1024;
    light.shadow.mapSize.height = 1024;
    var d = 300;
    light.shadow.camera.left = -d;
    light.shadow.camera.right = d;
    light.shadow.camera.top = d;
    light.shadow.camera.bottom = -d;
    light.shadow.camera.far = 1000;
    scene.add(ambientLight);
    scene.add(pointLight);
    //scene.add(new THREE.AmbientLight(0x666666));
    scene.add(light);
}

function init_grass() {
    var loader = new THREE.TextureLoader();
    var groundTexture = loader.load(loader_options.grass_texture);
    groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
    groundTexture.repeat.set(128, 128);
    groundTexture.anisotropy = 16;
    var groundMaterial = new THREE.MeshLambertMaterial({
        map: groundTexture
    });
    var mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(5000, 5000), groundMaterial);
    mesh.position.y = 0;
    mesh.position.z = 0;
    mesh.rotation.x = -Math.PI / 2;
    mesh.receiveShadow = true;
    scene.add(mesh);
}

function init_house() {
    new THREE.MTLLoader()
        .load(loader_options.house_mtl, function (house_materials) {
            house_materials.preload();
            new THREE.OBJLoader()
                .setMaterials(house_materials)
                .load(loader_options.house_obj, function (house) {
                    house.position.x = 0;
                    house.position.y = 1;
                    house.position.z = 0;
                    scene.add(house);
                });
        });
}

function init_clouds() {
    let noiseTexture = new THREE.TextureLoader().load(loader_options.perlin_noise);
    noiseTexture.wrapS = noiseTexture.wrapT = THREE.RepeatWrapping;
    noiseTexture.repeat.set(512, 512);
    noiseTexture.anisotropy = 16;
    cloud_options.texture.value = noiseTexture;

    let groundMaterial2 = new THREE.MeshLambertMaterial({
        map: noiseTexture
    });
    let mesh2 = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), groundMaterial2);
    scene.add(mesh2);


    let shaderMaterial = new THREE.ShaderMaterial({
        uniforms: cloud_options,
        vertexShader: document.getElementById('vertexshader').textContent,
        fragmentShader: document.getElementById('fragmentshader').textContent
    });

    clouds = new THREE.Mesh(new THREE.PlaneBufferGeometry(5120, 5120), shaderMaterial);
    clouds.position.x = 0;
    clouds.position.z = 0;
    clouds.rotation.x = Math.PI / 2;
    clouds.position.y = 500;
    scene.add(clouds);
}

// Метод инициализации окуржения
function init() {
    if (WEBGL.isWebGLAvailable() === false) {
        document.body.appendChild(WEBGL.getWebGLErrorMessage());
    }

    container = document.createElement('div');
    document.body.appendChild(container);

    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    container.appendChild(renderer.domElement);

    // Инициализация сцены
    scene = new THREE.Scene();
    // Включение тумана
    if (params.fog.foggy) {
        scene.fog = new THREE.Fog(0xa4c5fc); // Цвет для туманного неба
        scene.background = new THREE.Color(0xa4c5fc);
    } else {
        scene.fog = null;
    }

    // Инициализация камеры
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 20000);
    camera.position.z = 500;
    camera.position.x = 0;
    camera.position.y = 200;
    scene.add(camera);

    clock = new THREE.Clock();
    tick = 0;

    // Настройки освещения
    init_light();
    // Дождь
    init_rain();
    // Загрузка травы
    init_grass();
    // Загрузка здания
    init_house();

    init_clouds();

    // Орбитное управление камерой
    var controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.target.copy(new THREE.Vector3(0, 0, 0));
    controls.update();

    // Установка приемников сигналов на события
    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener("keydown", onDocumentKeyDown, false);
}

function recalc_color() {
    if (params.fog.foggy) {
        scene.fog.color = scene.background; // Цвет для туманного неба
    }
    if (params.cloudy) {
        cloud_options.backgroundColor.value = scene.background;
        if (params.day) {
            cloud_options.cloudColor.value = new THREE.Color(0xffffff);
        } else {
            cloud_options.cloudColor.value = new THREE.Color(0x999999);
        }
    }
}

function build_gui() {
    gui = new dat.GUI();
    var fogfolder = gui.addFolder('Параметры тумана');
    fogfolder.add(params.fog, 'foggy').name('Fog').onChange(function (value) {
        if (params.fog.foggy) {
            scene.fog = new THREE.Fog(scene.background, params.fog.near, params.fog.far); // Цвет для туманного неба
        } else {
            scene.fog = null;
        }
        recalc_color();
    });
    fogfolder.add( params.fog, 'near' ).name('Near').min(1).max(200).step(1).onChange(function (value) {
        if (params.fog.foggy) {
            scene.fog = new THREE.Fog(scene.background, params.fog.near, params.fog.far); // Цвет для туманного неба
        }
    });
    fogfolder.add( params.fog, 'far' ).name('Far').min(200).max(1000).step(1).onChange(function (value) {
        if (params.fog.foggy) {
            scene.fog = new THREE.Fog(scene.background, params.fog.near, params.fog.far); // Цвет для туманного неба
        }
    });
    fogfolder.open();

    var rainFolder = gui.addFolder('Параметры дождя');
    rainFolder.add(params.rain, 'rainy').name('Rain').onChange(function (value) {
        if (params.rain.rainy) {
            scene.add(rainParticles);
            scene.background = new THREE.Color(0x313A47); // Цвет для дождливого неба
        } else {
            scene.remove(rainParticles);
        }
        recalc_color();
    });
    rainFolder.add( params.rain, 'positionRandomness' ).name('Area').min(100).max(1000).step(1).listen();
    rainFolder.add( params.rain, 'turbulence' ).name('Turbulence').min(0).max(10).step(0.1).listen();
    rainFolder.add( params.rain, 'lifetime' ).name('Lifetime').min(0).max(5).step(0.1).listen();
    rainFolder.add( params.rain, 'size' ).name('Size').min(1).max(20).step(1).listen();
    rainFolder.add( params.rain.spawnerOptions, 'spawnRate' ).name('spawnRate').min(1000).max(90000).step(1000).onChange(function (value) {
        params.rain.maxParticles = params.rain.spawnerOptions.spawnRate * params.rain.lifetime;
        if (params.rain.rainy) scene.remove(rainParticles);
        rainParticles = new Rain({
            maxParticles: params.rain.maxParticles,
        });
        if (params.rain.rainy) scene.add(rainParticles);
    });
    rainFolder.addColor(params.rain, 'color').name('Color');

    var cloudsFolder = gui.addFolder('Параметры  облаков');
    cloudsFolder.add(params, 'cloudy').name('Clouds').onChange(function (value) {
        if (params.cloudy) {
            scene.add(clouds);
        } else {
            scene.remove(clouds);
        }
    });
    cloudsFolder.add( cloud_options.scale, 'value' ).name('Scale').min(0).max(5).step(0.1).listen();
    cloudsFolder.add( cloud_options.red_weight, 'value' ).name('red_weight').min(0).max(1).step(0.1).listen();
    cloudsFolder.add( cloud_options.green_weight, 'value' ).name('green_weight').min(0).max(1).step(0.1).listen();
    cloudsFolder.add( cloud_options.blue_weight, 'value' ).name('blue_weight').min(0).max(1).step(0.1).listen();
    cloudsFolder.addColor(cloud_options.cloudColor, 'value').name('Color');

    var dayFolder = gui.addFolder('Параметры  вермени суток');
    dayFolder.add(params, 'day').name('Day').onChange(function (value) {
        if (params.day) {
            scene.background = new THREE.Color(0xa4c5fc);
        } else {
            scene.background = new THREE.Color(0x111F28);
        }
        recalc_color();
    });
    dayFolder.addColor(scene, 'background').name('Background color');
    dayFolder.add( cloud_options.blue_weight, 'value' ).name('Ambient Light').min(0).max(2).step(0.1).listen();

    gui.open();
}

// Обработчик событий на нажатие клавиш
function onDocumentKeyDown(event) {
    var keyCode = event.which;
    if (keyCode == 87) {
        house.position.y += 1;
    } else if (keyCode == 83) {
        house.position.y -= 1;
    } else if (keyCode == 65) {
        house.position.x -= 1;
    } else if (keyCode == 68) {
        house.position.x += 1;
    } else if (keyCode == 32) {
        house.position.set(0, 0, 0);
    }
};

// Обработчик событий на изменение размера окна
function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function onDocumentMouseMove(event) {
    mouseX = (event.clientX - windowHalfX) / 2;
    mouseY = (event.clientY - windowHalfY) / 2;
}

// Метод анимации loop
function animate() {
    requestAnimationFrame(animate);

    var delta = clock.getDelta() * params.rain.spawnerOptions.timeScale;
    tick += delta;
    if (tick < 0) tick = 0;
    if (delta > 0) {
        for (var x = 0; x < params.rain.spawnerOptions.spawnRate * delta; x++) {
            rainParticles.spawnParticle(params.rain);
        }
    }
    rainParticles.update(tick);

    var time = tick;
    //var time = 1.0;
    cloud_options.time.value = time * 0.1;

    render();
}

// Метод отрисовки сцены
function render() {
    camera.lookAt(scene.position);
    renderer.render(scene, camera);
}
