FROM python:latest

COPY ./app /app
WORKDIR /app

EXPOSE 8000

CMD ["python3","-m", "http.server"]
